<link rel="stylesheet" href="styles/navbar.css">


<nav class="navbar navbar-light bg-light fixed-top">
    <!--       <i class="fas fa-bars"></i>        -->
    <div class="coaching-nav">
        <a href="#coaching-container" class="nav-link">
            <p>Coaching</p>
        </a>
    </div>

    <!--    <div class="mx-auto">
            <ul class="navbar-nav" id="menu-list" style="color: #222222;">
                <li><a href="https://www.facebook.com/YannRocheteauFanPage/" target="_blank"
                       class="fab fa-facebook fa-lg nav-link"></a></li>
                <li><a href="https://www.facebook.com/YannRocheteauFanPage/" target="_blank"
                       class="fab fa-twitter fa-lg nav-link"></a></li>
                <li><a href="https://www.facebook.com/YannRocheteauFanPage/" target="_blank"
                       class="fab fa-instagram fa-lg nav-link"></a></li>
                <li><a href="https://www.facebook.com/YannRocheteauFanPage/" target="_blank"
                       class="fab fa-linkedin-in fa-lg nav-link"></a></li>
            </ul>
        </div>-->

    <div class="navbar-center" id="coaching-btn">
        <a class="navbar-brand" href="index.php" style="font-size: 26px">Yann Rocheteau <em style="font-size: 18px">Triathlète professionnel</em></a>
    </div>


    <button class="navbar-toggler ml-auto" id="navbar-toggler" type="button" data-toggle="collapse"
            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
            aria-label="Toggle navigation" onclick="">
        <span class="navbar-toggler-icon"></span>
    </button>


    <!--        <script>

                window.addEventListener("load", function () {
                    let menu = document.getElementById("menu-list");

                    if (window.innerWidth > 992) {
                        menu.classList.add("navbar-center")
                    } else {
                        menu.classList.remove("navbar-center");
                    }
                })

                window.addEventListener("resize", function () {
                    let menu = document.getElementById("menu-list");
                    if (window.innerWidth > 992) {
                        menu.classList.add("navbar-center");
                    } else {
                        menu.classList.remove("navbar-center");
                    }
                });
            </script>-->
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav" id="menu-list">
            <li>
                <a href="#partner-container" class="nav-link">
                    <p>Partenaires</p>
                </a>
            </li>
            <li>
                <a href="" class="nav-link">
                    <p>Actus</p>
                </a>
            </li>
            <li>
                <a href="palmares.php" class="nav-link">
                    <p>Palmares</p>
                </a>
            </li>
            <li>
                <a href="#results-anchor" class="nav-link">
                    <p>Resultats</p>
                </a>
            </li>
            <li>
                <a href="#bio-anchor" class="nav-link">
                    <p>Bio Athlète</p>
                </a>
            </li>
            <li>
                <a href="#objectif-anchor" class="nav-link">
                    <p>Objectif Saison</p>
                </a>
            </li>
            <li class="dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
                   aria-expanded="false">Espace Membre</a>
                <div class="dropdown-menu">
                    <button class="dropdown-item btn" type="button" data-target="#connectionModal" data-toggle="modal"
                            disabled>
                        Se connecter
                    </button>
                    <button class="dropdown-item btn" type="button" data-target="#connectionModal" data-toggle="modal"
                            disabled>
                        S'inscrire
                    </button>
                    <div role="separator" class="dropdown-divider"></div>
                    <a class="dropdown-item" href="contact.php">Contact</a>
                </div>
            </li>
        </ul>
    </div>
</nav>

<div class="modal" autofocus tabindex="-1" role="dialog" id="connectionModal" aria-labelledby="connectionModal"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Se connecter</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email ou Nom d'utilisateur</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                               placeholder="Enter email or Username">
                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone
                            else.</small>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Mot de Passe</label>
                        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                    </div>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                        <label class="form-check-label" for="exampleCheck1">Se souvenir de moi</label>
                    </div>
                    <div class="row" style="padding-left: 20px">
                        <div class="offset-9">
                            <button type="submit" class="btn btn-primary">Connection</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>