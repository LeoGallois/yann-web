(function () {
    window.addEventListener("resize", function (){
        let menu = document.getElementById("menu-list");
        if (window.innerWidth > 992){
            menu.classList.add("navbar-center")
        } else{
            menu.classList.remove("navbar-center");
        }
    });
})