;(function ($, window, document, undefined) {
    $(document).ready(function () {
        $('#image-map-pro-container').imageMapPro({
            "id": 1644,
            "editor": {"selected_shape": "spot-2519", "tool": "select", "shapeCounter": {"spots": 7}},
            "general": {
                "name": "Untitled",
                "shortcode": "Untitled",
                "width": 1603,
                "height": 742,
                "naturalWidth": 1603,
                "naturalHeight": 742
            },
            "image": {"url": "https://upload.wikimedia.org/wikipedia/commons/a/ac/Simplified_blank_world_map_without_Antartica_%28no_borders%29.svg"},
            "spots": [{
                "id": "spot-6160",
                "title": "NA",
                "x": 7.463,
                "y": 32.159,
                "x_image_background": 7.463,
                "y_image_background": 32.159,
                "default_style": {"icon_fill": "#FF1629"},
                "mouseover_style": {"icon_fill": "#FFC634"},
                "tooltip_style": {"position": "right", "offset_x": -1.6843031496062935, "offset_y": 1.8653915496706794},
                "tooltip_content": {
                    "squares_settings": {
                        "containers": [{
                            "id": "sq-container-576501",
                            "settings": {
                                "elements": [{
                                    "settings": {"name": "Image", "iconClass": "fa fa-camera"},
                                    "options": {
                                        "heading": {"text": "My Shape"},
                                        "image": {
                                            "url": "http://157.245.20.183/images/IronMan/NT-Flyer.png",
                                            "link_to": "https://www.ironman.com/im-st-george"
                                        },
                                        "general": {"css": "width: 1000px;"}
                                    }
                                }]
                            }
                        }]
                    }
                }
            }, {
                "id": "spot-2519",
                "title": "Les Sables D'olonne",
                "x": 40.801,
                "y": 24.147,
                "x_image_background": 40.801,
                "y_image_background": 24.147,
                "default_style": {"icon_fill": "#FF1629"},
                "mouseover_style": {"icon_fill": "#FFC634"},
                "tooltip_style": {
                    "position": "bottom",
                    "offset_x": 0.08637010221464436,
                    "offset_y": -1.7197435633698746
                },
                "tooltip_content": {
                    "squares_settings": {
                        "containers": [{
                            "id": "sq-container-726911",
                            "settings": {
                                "elements": [{
                                    "settings": {"name": "Image", "iconClass": "fa fa-camera"},
                                    "options": {
                                        "image": {
                                            "url": "http://157.245.20.183/images/IronMan/sables-date.png",
                                            "image_is_a_link": 1,
                                            "link_to": "https://www.ironman.com/im703-les-sables-dolonne"
                                        }, "style": {"border_radius": 10}, "general": {"css": "width: 1000px;"}
                                    }
                                }]
                            }
                        }]
                    }
                }
            }, {
                "id": "spot-4287",
                "title": "Arizona",
                "x": 9.408,
                "y": 37.113,
                "x_image_background": 9.408,
                "y_image_background": 37.113,
                "default_style": {"icon_fill": "#FF1629"},
                "mouseover_style": {"icon_fill": "#FFC634"},
                "tooltip_style": {"position": "right", "offset_x": -1.4480068143100553, "offset_y": 2.2226110820931524},
                "tooltip_content": {
                    "squares_settings": {
                        "containers": [{
                            "id": "sq-container-118931",
                            "settings": {
                                "elements": [{
                                    "settings": {"name": "Image", "iconClass": "fa fa-camera"},
                                    "options": {
                                        "heading": {"text": "My Shape"},
                                        "image": {
                                            "url": "http://157.245.20.183/images/IronMan/Arizona-flyer.png",
                                            "link_to": "https://www.ironman.com/im-arizona"
                                        },
                                        "general": {"css": "width: 1000px;"}
                                    }
                                }]
                            }
                        }]
                    }
                }
            }, {
                "id": "spot-8958",
                "title": "Finland",
                "x": 48.382,
                "y": 13.29,
                "x_image_background": 48.382,
                "y_image_background": 13.29,
                "default_style": {"icon_fill": "#FF1629"},
                "mouseover_style": {"icon_fill": "#FFC634"},
                "tooltip_style": {
                    "position": "bottom",
                    "offset_x": -0.027546259842516463,
                    "offset_y": -1.2340063543059472
                },
                "tooltip_content": {
                    "squares_settings": {
                        "containers": [{
                            "id": "sq-container-41311",
                            "settings": {
                                "elements": [{
                                    "settings": {"name": "Image", "iconClass": "fa fa-camera"},
                                    "options": {
                                        "heading": {"text": "My Shape"},
                                        "image": {
                                            "url": "http://157.245.20.183/images/IronMan/finland-flyer.png",
                                            "image_is_a_link": 1,
                                            "link_to": "https://www.ironman.com/im-Finland"
                                        },
                                        "general": {"css": "width: 1000px;"}
                                    }
                                }]
                            }
                        }]
                    }
                }
            }]
        });
    });
})(jQuery, window, document);