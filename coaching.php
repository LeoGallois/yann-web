<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="styles/coaching.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">

    <title>Yann</title>
    <script src="js/chart.js" type="text/javascript"></script>
    <script src="js/popover.js" type="text/javascript"></script>
    <script src="https://kit.fontawesome.com/473624bd8f.js" crossorigin="anonymous"></script>
</head>
<?php include("navbar.php"); ?>

<body class="">

<div class="row">
    <div class="col-6">
        <div class="offset-3 col-8">
            <div class="card">
                <div class="card-body">
                    <div class="text-center">
                        <h2 class="card-title ">ALL ROUND</h2>
                        <ul class="coaching-list">
                            <li>Coaching individualisé, adapté aux objectifs, au profil sportif, aux disponibilités de
                                l’athlète.
                            </li>
                            <li>Entretien initial (physique ou visioconférence)</li>
                            <li>Préparation d’un carnet d’entraînement à la semaine (Application Nolio)</li>
                            <li>Suivi et ajustement hebdomadaire de la progression</li>
                            <li>Analyse et débriefing 1 / mois</li>
                            <li>Échanges réguliers d’informations</li>
                            <li>Conseils et partage d’expérience (nutrition, matériel, choix des courses, logistique
                                voyages, bons plans)
                            </li>
                        </ul>
                        <h4>90€/MOIS</h4>
                        <p>Sur un an</p>
                        <a href="contact.php" class="nav-link"><h3>S'inscrire</h3></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-6">
        <div class="offset-1 col-8">
            <div class="card">
                <div class="card-body">
                    <div class="text-center">
                        <h2 class="card-title ">ONE SHOT </h2>
                        <ul class="coaching-list">
                            <li>Coaching individualisé, adapté aux objectifs, au profil sportif, aux disponibilités de
                                l’athlète.
                            </li>
                            <li>Entretien initial (physique ou visioconférence)</li>
                            <li>Préparation d’un carnet d’entraînement à la semaine (Application Nolio)</li>
                            <li>Suivi et ajustement hebdomadaire de la progression</li>
                            <li>Analyse et débriefing 1 / mois</li>
                            <li>Échanges réguliers d’informations</li>
                            <li>Conseils et partage d’expérience (nutrition, matériel, choix des courses, logistique
                                voyages, bons plans)
                            </li>
                        </ul>
                        <h4>99€ / mois</h4>
                        <p>Sur 3 mois minimum</p>
                        <a href="contact.php" class="nav-link"><h3>S'inscrire</h3></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
