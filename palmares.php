<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="styles/palmares.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">

    <title>Yann</title>
    <script src="js/chart.js" type="text/javascript"></script>
    <script src="js/popover.js" type="text/javascript"></script>
    <script src="https://kit.fontawesome.com/473624bd8f.js" crossorigin="anonymous"></script>
</head>
<?php include("navbar.php"); ?>

<body class="container-fluid nopadding">
    <div class="palmares-container">
        <ul class="palmares-list">
            <h5>2019</h5>
            <li>- IronMan Lanzarote (25/05/2019): 22ème Pro, 45ème Général en 10h10</li>
            <li>- 70.3 Les Sables d'Olonne (16/06/2019): 14ème Pro, 32ème Général en  4h23</li>
            <li>- IronMan Lake Placid (28/07/2019): 22ème Pro, 183ème Général en 11h27</li>
            <li>- Victoire Half Occitaman Toulous (14/09/2019) en 4h45</li>
            <li>- IronMan Busselton (01/12/2019) : 17ème Pro, 102ème au général en 9h52</li>
            <br>
            <h5>2018</h5>
            <li>- 210ème aux Championnats du Monde IRONMAN Hawaii, 24ème en groupe d’âge 40/44 (7ème qualification consécutive)</li>
            <li>- 7ème à l’IM de Santa Rosa (Californie, USA), +4% du vainqueur, 2ème groupe d’âge 40/44</li>
            <li>- 2ème au Triathlon Les Sables d'Olonne (distance olympique)</li>
            <br>
            <h5>2017</h5>
            <li>- 284ème aux Championnats du Monde IRONMAN Hawaii, 261ème en groupe d’âge 35/39</li>
            <li>- 10ème à l’IM de Boulder (Colorado, USA), +10,95% du vainqueur, 1er groupe d’âge 35/39</li>
            <li>- 25ème au Triathlon LD Alpe d’Huez</li>
            <li>- 2ème au Triathlon Les Sables d'Olonne (distance olympique)</li>
            <br>
            <h5>2016</h5>
            <li>- 97ème aux Championnats du Monde IRONMAN Hawaii, 21ème en groupe d’âge 35/39</li>
            <li>- 2ème à l’Half IronMan de St Jean de Luz</li>
            <li>- 5ème à l’IM de Lanzarote en groupe d’âge 35/39 (33ème au général), +11% du vainqueur</li>
            <li>- 1er au Triathlon Les Sables d'Olonne (distance olympique)</li>
            <br>
            <h5>2015</h5>
            <li>- Participation aux Championnats du Monde IRONMAN Hawaii</li>
            <li>- 1er à l’IRONMAN d’Afrique du Sud en groupe d’âge 35/39 (24ème au général)</li>
            <li>- Vice-champion Français de Triathlon Longue Distance en groupe d’âge 35/39</li>
            <br>
            <h5>2014</h5>
            <li>- Participation aux Championnats du Monde IRONMAN Hawaii</li>
            <li>- 1er à l’IM de Mont-Tremblant, Championnats Nord-Américains en 30/34 (9ème au général)</li>
            <li>- 2ème à l’IM 70.3 de Majorque en groupe d’âge 30/34 (39ème au général)</li>
            <li>- 2ème à l’IRONMédoc et record personnel en 8h42</li>
            <br>
            <h5>2013</h5>
            <li>- Participation aux Championnats du Monde IRONMAN Hawaii</li>
            <li>- 2ème à l’IM de Lanzarote en groupe d’âge 30/34 (14ème au général)</li>
            <br>
            <h5>2012</h5>
            <li>- Participation aux Championnats du Monde IRONMAN Hawaii</li>
            <li>- Champion de France de Triathlon Longue Distance en groupe d’âge 30/34</li>
            <br>
            <h5>2011</h5>
            <li>- Participation aux Championnats du Monde IRONMAN 70.3 à Las Vegas</li>
            <br>
            <h5>2009</h5>
            <li>- Champion de France de Triathlon Longue Distance en groupe d’âge 30/34</li>
        </ul>
    </div>
</body>