<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="styles/index.css">
    <link rel="stylesheet" href="styles/image-map-pro.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">

    <title>Yann</title>
    <script src="js/chart.js" type="text/javascript"></script>
    <script src="js/popover.js" type="text/javascript"></script>
    <script src="https://kit.fontawesome.com/473624bd8f.js" crossorigin="anonymous"></script>

    <script type="text/javascript" src="js/jquery.onepage-scroll.js"></script>
    <link href='styles/onepage-scroll.css' rel='stylesheet' type='text/css'>
</head>
<?php include("navbar.php"); ?>

<body class="nopadding">

<div class="parallax1">
    <div class="row">
        <div class="col-md-12">
            <h1></h1>
        </div>
    </div>
</div>

<div class="white partner-container" id="partner-container">
    <h2 class="partner-h">Partenaires</h2>
    <div class="row partner" style="text-align: center">

        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
            <a target="_blank" href="https://cycles-leon.com/"><img src="images/brand/cycles_leon_logo_noir.png"
                                                                    class="img-fluid brand"
                                                                    alt="cycles leon partenaire"></a>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
            <a target="_blank" href="https://www.lessablesdolonne.fr/"><img
                        src="images/brand/les-sables-d'olonne.png" class="img-fluid brand"
                        alt="Les Sables d'olonne partenaire"></a>
        </div>
        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
            <a target="_blank" href="https://www.ocea.fr/"><img src="images/brand/ocea.png" class="img-fluid brand"
                                                                alt="ocea partenaire"></a>
        </div>
        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
            <a target="_blank" href="https://www.paso-traiteur.com/"><img src="images/brand/paso.png"
                                                                          class="img-fluid brand"
                                                                          alt="paso partenaire"></a>
        </div>
    </div>
</div>

<div class="parallax-bio">
    <div class="row">
        <div class="offset-1 col-5 bio">
            <div>
                <ul class="bio-list">
                    <li>« Le triathlon pour moi, c’est un mode de vie, une invitation aux voyages, une passion que je
                        cultive depuis plus de 20 ans.
                    </li>
                    <li>Basé aux Sables d’Olonne depuis mes débuts, la Vendée a toujours été une terre de triathlon, le
                        destin était tracé !
                    </li>
                    <li>L’expérience m’a amené au fur et à mesure de mon évolution à conjuguer 2 casquettes : Triathlète
                        Professionnel et Coach en Triathlon.»
                    </li>
                </ul>
            </div>
        </div>
        <div class="bio">
            <ul class="bio-list">
                <li>Taille 1.80m</li>
                <li>Poids entre 63kg/65kg</li>
                <li>FC Repos 36</li>
                <li>FC Max 179</li>
                <li>Né le 23/12/1978 aux Sables d’Olonne (85)</li>
                <li>Vit aux Sables d’Olonne</li>
                <li>Nationalité Française</li>
                <li>Entraîneur Stéphane Palazzetti (depuis 2013)</li>
                <li>«Merci à toute l’équipe nécessaire à la bonne réalisation de ces projet »</li>
            </ul>
        </div>
    </div>
</div>

<div class="parallax2"></div>

<div class="parallax3">
    <a id="results-anchor" style="position:relative; top: -200px"></a>
    <div id="results">
        <div class="row justify-content-center">
            <h2>Performances</h2>
        </div>
        <div class="row results-stats">
            <div class="col-lg-3 col-sm-6 col-xs-12">
                <div>7</div>
                <p>Participations consécutives IRONMAN World Champion ship Kona Hawaii</p>
            </div>
            <div class="col-lg-3 col-sm-6 col-xs-12">
                <div>23</div>
                <p>courses distance IRONMAN</p>
            </div>
            <div class="col-lg-3 col-sm-6 col-xs-12">
                <div>7</div>
                <p>podiums en groupe d’âge sur circuit IRONMAN</p>
            </div>
            <div class="col-lg-3 col-sm-6 col-xs-12">
                <div>2</div>
                <p>victoires aux Championnats de France en groupe d’âge (Triathlon longue distance) </p>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="palmares">
                <a class="" href="/palmares"><h5 class="palmares-text">Voir mon Palmarès</h5></a>
            </div>
        </div>
    </div>
</div>

<div class="white partner-container" id="partner-container">
    <h2 class="partner-h">Mes équipements</h2>
    <div class="row partner" style="text-align: center">
        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
            <a target="_blank" href="https://www.bdo.fr/fr-fr/accueil"><img src="images/brand/bdo.png"
                                                                            class="img-fluid brand"
                                                                            alt="bdo partenaire"></a>
        </div>
        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
            <a target="_blank" href="https://www.magestionlocative.fr/"><img src="images/brand/logo-ma-gestion-locative.jpg"
                                                                            class="img-fluid brand"
                                                                            alt="bdo partenaire"></a>
        </div>
        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
            <a target="_blank"
               href="https://all.accor.com/hotel/9431/index.fr.shtml?utm_campaign=seo+maps&utm_medium=seo+maps&utm_source=google+Maps"><img
                        src="images/brand/ibis.png" class="img-fluid brand" alt="ibis partenaire"></a>
        </div>
        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
            <a target="_blank" href="https://www.stamp.yt/fr"><img src="images/brand/stampyt.png"
                                                                   class="img-fluid brand" alt="stampyt partenaire"></a>
        </div>
    </div>
</div>

<div class="parallax4"></div>

<div id="coaching-container" class="container-fluid">

    <div class="row justify-content-center">
        <h2>Programmes</h2>
    </div>

    <div class="row">
        <div class="offset-2 col-4">
            <div class="card">
                <div class="card-body">
                    <div class="text-center">
                        <h2 class="card-title ">ALL ROUND</h2>
                        <h4>90€/MOIS</h4>
                        <p>Sur un an</p>
                        <a href="contact.php" class="nav-link"><h3>S'inscrire</h3></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="card">
                <div class="card-body">
                    <div class="text-center">
                        <h2 class="card-title ">ONE SHOT</h2>
                        <h4>99€/MOIS</h4>
                        <p>Sur 3 mois minimum</p>
                        <a href="contact.php" class="nav-link"><h3>S'inscrire</h3></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="offset-2 col-4">
            <div class="card">
                <div class="card-body">
                    <div class="text-center">
                        <h2 class="card-title ">STAGES</h2>
                        <p>À partir de 195 € / 1 semaine selon le site d’accueil</p>
                        <a href="coaching.php" class="nav-link"><h3>Plus d'info</h3></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="card">
                <div class="card-body">
                    <div class="text-center">
                        <h2 class="card-title ">SPORT EN ENTREPRISE</h2>
                        <p>Sur mesure, en fonction des besoins</p>
                        <a href="coaching.php" class="nav-link"><h3>Plus d'info</h3></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!--    <li style="list-style-type: none; padding-inline-start: 0px" class="card-text">
        <ul>Entretien physique</ul>
        <ul>téléphonique</ul>
        <ul>Utilisations du logiciel Nolio</ul>
        <ul>Adaptation du planning toutes les semaines</ul>
        <ul>Analyse et débriefing une fois par mois</ul>
        <ul>Contact/infos/échanges par sms et ou reseaux</ul>
        <ul>Conseils/expériences/contactes (entraînements, nutrition, matériel, sélection
            objectifs,
            logistiques voyages courses)
        </ul>
    </li>-->

</div>


<div class="white partner-container" id="partner-container">
    <h2 class="partner-h">Donations de Matériels</h2>
    <div class="row partner" style="text-align: center">
        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
            <a target="_blank" href="http://www.rhinocsport.com/fr/"><img src="images/brand/rhinoc-sport.png"
                                                                          class="img-fluid brand"
                                                                          alt="rhinoc sport partenaire"></a>
        </div>
        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
            <a target="_blank" href="https://www.facebook.com/tartineetgourmandise.lesSablesDOlonne/"><img
                        src="images/brand/tartine-et-gourmandise.png" class="img-fluid brand"
                        alt="Tartine et Gourmandise partenaire"></a>
        </div>
        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
            <a target="_blank" href="http://www.unzestedebienetre.com/"><img
                        src="images/brand/un-zest-de-bien-etre.webp" class="img-fluid brand"
                        alt="Un Zest de bien être partenaire"></a>
        </div>
        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
            <a target="_blank" href="https://www.veets.fr/"><img src="images/brand/veets.png"
                                                                 class="img-fluid brand" alt="veets partenaire"></a>
        </div>
        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
            <a href="https://www.la-ptite-cale.fr/"><img src="images/brand/la-ptite-cale.png"
                                                         class="img-fluid brand"
                                                         alt="La petite Cale partenaire"></a>
        </div>
        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
            <a target="_blank" href="https://www.marminpaysage.fr/"><img src="images/brand/logo-marmin.png"
                                                                         class="img-fluid brand"
                                                                         alt="marmin partenaire"></a>
        </div>
        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
            <a target="_blank" href="https://www.mx3.fr/en/"><img src="images/brand/mx3-extreme.png"
                                                                  class="img-fluid brand"
                                                                  alt="mx3 extreme partenaire"></a>
        </div>
        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
            <a target="_blank" href="http://www.aquasphereswim.com/us/"><img src="images/brand/aqua-sphere.png"
                                                                             class="img-fluid brand"
                                                                             alt="aquasphere parternaire"></a>
        </div>
        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
            <a target="_blank" href="http://www.bvsport.com/en/"><img src="images/brand/bv-sport.png"
                                                                      class="img-fluid brand"
                                                                      alt="bv sport parternaire"></a>
        </div>
        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
            <a target="_blank" href="https://www.caseproof.net/fr"><img src="images/brand/caseproof.png"
                                                                        class="img-fluid brand"
                                                                        alt="caseproof partenaire"></a>
        </div>
        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
            <a target="_blank" href="https://crampfix.com.au/"><img src="images/brand/crampfix.png"
                                                                    class="img-fluid brand"
                                                                    alt="crampfix partenaire"></a>
        </div>
        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
            <a target="_blank" href="https://www.cyclingceramic.fr/"><img src="images/brand/cyclingceramic.png"
                                                                          class="img-fluid brand"
                                                                          alt="cyclingceramic partenaire"></a>
        </div>
        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
            <a target="_blank" href="https://www.facebook.com/Running-les-Sables-829675453786115/"><img
                        src="images/brand/running-les-sables.png" class="img-fluid brand"
                        alt="running les sables partenaire"></a>
        </div>
    </div>
</div>

<div class="bg-diagonale" id="bio-athlete">
    <div class="row">
        <div class="col-12">
            <div class="col-6">

            </div>
            <div class="col-6"></div>
        </div>
    </div>
</div>

<div class="map-container">
    <div class="row justify-content-center">
        <h2>Prochaines Courses</h2>
    </div>

    <div class="row">
        <div class="col-10 mx-auto">
            <div id="image-map-pro-container"></div>
        </div>
    </div>
</div>
<div></div>

<footer class="" style="min-height: 150px; background-color: #7b7b7b">
    <div class="row">
        <div class="col-12 footer-info"></div>
    </div>
    <div class="row justify-content-center" style="vertical-align: middle">
        <div class="col-12" id="footer-mentions">
            <p>© Yann Rocheatau. Tous droits réservés.</p>
        </div>
    </div>
</footer>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://getbootstrap.com/docs/4.1/assets/js/vendor/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>
<script src="js/image-map-pro-min.js"></script>
<script src="js/map.js"></script>
</body>
</html>