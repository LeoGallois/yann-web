<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="styles/navbar.css">
    <link rel="stylesheet" href="styles/contact.css">

    <title>Title</title>
</head>
<?php include("navbar.php"); ?>

<body>
<div class="container-fluid nopadding">
    <div class="row justify-content-center">
        <div class="contact-form-container col-8">
            <form action="">
                <div class="form-group row">
                    <div class="col">
                        <input type="text" class="form-control" placeholder="Nom">
                    </div>
                    <div class="col">
                        <input type="text" class="form-control" placeholder="Prénom">
                    </div>
                </div>
                <div class="form-group">
                    <label for="emailInput">Adresse Mail</label>
                    <input type="email" class="form-control" id="emailInput" aria-describedby="email"
                           placeholder="email">
                </div>
                <div class="form-group">
                    <label for="phoneNumber">Numéro de Téléphone</label>
                    <input type="tel" class="form-control" id="phoneNumber" aria-describedby="phone number"
                           placeholder="Téléphone">
                </div>
                <div class="form-group">
                    <label for="textAreaInput">Message</label>
                    <textarea class="form-control" name="message" id="textAreaInput" cols="30" rows="10"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Envoyer</button>
            </form>
        </div>
    </div>

    <button type="button" class="btn btn-secondary myPopover" hidden data-container="body" data-toggle="popover" data-placement="top" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
        Popover on top
    </button>
</div>


<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://getbootstrap.com/docs/4.1/assets/js/vendor/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>

<script>
    $(function(){
        $(".myPopover").popover();
    });
</script>

</body>
</html>